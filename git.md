### **GIT**

Eu utilizo o [GitLab](https://gitlab.com/) como repositório de códigos de meus projetos. Porém, o GitLab é uma versão online e para nos comunicar com ela, utilizamos o [Git](https://git-scm.com/). 

Imagine que você está compartilhando um código com alguém por meio de enviando arquivos por e-mail ou uma pasta no Google Drive, Dropbox ou Onedrive. É preciso um grande esforço para ter certeza de que vocês estarão com a mesma versão de arquivo ou até mesmo se não estão sobrescrevendo o trabalho um do outro. Com Git e GitLab, vocês conseguem trabalhar no mermo arquivo ao mesmo tempo. O Git e o GitLab combinarão as mudanças automaticamente ou mostrará para você os conflitos e ambiguidades.

Nesta página, você aprenderá como instalar e configurar o Git em seu computador e comandos básicos para trabalhar com o Git. 

##### **O QUE É**

[Git](https://git-scm.com/) é um sistema de controle de versão de arquivos e códigos. Através dele é possível desenvolver projetos no qual diversas pessoas podem contribuir simultaneamente, editando e criando novos arquivos. Assim, o [Git](https://git-scm.com/) permite que os arquivos e códigos possam existir sem o risco de suas alterações serem sobrescritas, além de possibilitar a recuperação de qualquer versão.

##### **USANDO O GIT**

Git faz com que seus arquivos sempre estejam em um dos três estados fundamentais: `consolidado (committed)`, `modificado (modified)` e `preparado (staged)`. Arquivos são ditos `consolidados` quando estão seguramente armazenados localmente. `Modificado` trata de um arquivo que sofreu mudanças, mas que ainda não foi consolidado localmente. Um arquivo é tido como `preparado` quando você marca um arquivo modificado em sua versão corrente para que ele faça parte do snapshot do próximo commit (consolidação). 

###### **PASSO A PASSO**

 1. Inicialmente devemos adicionar o repositório de um projeto que queremos trabalhar localmente. 

 2. Acesse o `Git Bash`, aperte o botão direito do mouse para colar o comando copiado (`git clone https://gitlab.com/hudson-shared/environment_config.git`) e aperte enter (o Git Bash pode solicitar suas credenciais do GitLab)

 3. Após o projeto ser copiado em seu computador, você deve acessá-lo digitando o seguinte comando `cd environment_config` que diz ao `Git Bash`que você quer entrar nesta pasta ou acessar a pasta da forma tradicional.

Pronto! Você tem o repositório `environment_config` no seu computador e pode trabalhar adicionando e/ou editando os arquivos deste repositório.

Abaixo, alguns cenários prováveis quando estamos trabalhando em equipe e como o Git e o GitLab podem nos ajudar a lidar com cada um deles:

* Alguém trabalhou no projeto desde a última vez que você o clonou e você não tem a última versão do projeto:
    * Para atualizar sua versão, acesse o projeto no Git Bash usando `cd environment_config` e use `git pull` para trazer a última versão do projeto para seu computador
* Você não se lembra de ter enviado as alterações que você fez no projeto para o GitLab ou não se recorda de quais arquivos você trabalhou
    * Acesse o projeto no Git Bash usando `cd environment_config` e utilize o comando `git status` para saber em quais arquivos foram feitas alterações
* Você alterou arquivos e quer enviar as mudanças para o GitLab
    * Acesse o projeto no Git Bash usando `cd environment_config`;
    * Use `git status "nome do arquivo"` para saber os arquivos que precisam ser enviados[^1];
    * Use `git add` para adicionar cada arquivo alterado[^2];
    * Use `git commit -m mensagem` para documentar as mudanças feitas[^3]
* Você modificou os arquivos, commitou descrevendo o que fez exatamente naquela modificação e agora precisa enviar tudo isso para o servidor. 
    * Acesse o projeto no Git Bash usando `cd environment_config`;
    * Use `git status "nome do arquivo"` para saber os arquivos que precisam ser enviados[^1];
    * Use `git add` para adicionar cada arquivo alterado[^2];
    * Use `git commit -m mensagem` para documentar as mudanças feitas[^3];
    * Use `git push -u origin master`[^4]
* Você removeu um arquivo do repositório e quer adicionar esta alteração no GitLab:
    * Acesse o projeto no Git Bash usando `cd environment_config`;
    * Use `git status "nome do arquivo"` para saber os arquivos que precisam ser enviados[^1];
    * Use `git rm arquivo_removido` para remover o arquivo;
    * Use `git commit -m mensagem` para documentar as mudanças feitas[^3];
    * Use `git push -u origin master`[^4]

Maiores detalhes sobre o uso do git e suas funcionalidades podem ser encontradas no livro [Pro Git](https://git-scm.com/book/pt-br/v1). Além disso, este [Guia Prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html) resume pontos importantes do Git. Para acessar o help do programa também pode-se utilizar:

```
git -- help
```

###### **OUTRAS INFORMAÇÕES ÚTEIS**

* Para ver o que está diferente desde seu último commit, usa-se o comando `git diff`. Outro uso do diff é para ver as diferenças nas mudanças dos arquivos que já foram preparados (staged)- os arquivos que você disse ao git que estão prontos para ser commitados- para isso utiliza-se o comando : `git diff with the --staged`.
* `git reset`: O comando `git reset` apenas desprepara o arquivo, mas ele continua lá, só não está preparado (staged). Os arquivos podem ser modificados para como eram no último commit através do comando: `git checkout -- <target>`
* `Git branch nome_do_branch`: cria um novo branch. Ao criar um novo branch você continua no branch anterior ( master). Para mudar de branch é preciso utilizar o comando:  `git checkout <branch>`
* Para juntar as modificações feitas em um branch para o branch principal, é preciso, inicialmente, ir para o branch principal através do comando `git checkout master`. Após, utiliza-se o comando `git merge nome_do_branch`.
* `git branch -d <nome_do_branch>` deleta um branch.
* Para deletar arquivos: `git rm nome-do-arquivo`. Já pata deletar todos os aquivos removidos ao mesmo tempo: `git ls-files --deleted | xargs git rm`. Nesse caso, por exemplo, se for deletar uma imagem, antes de executar esses comandos, deve-se deletar, na pasta imagens salvo no projeto do seu computador, o arquivo desejado, após seguir com esses comandos no Git Bash. 


[^1]: Esse comando mostrará os arquivos com status tracked ( que são aqueles que já estão inseridos no repositório) os unmodified (que não foram modificados por você), modified (que foram modificados por você) ou staged (que são os arquivos que acabaram de ser mudados).
[^2]: para adiconar uma pasta inteira basta adicionar o nome da pasta + * ( ex: Imagens/*)
[^3]: Ao commitar e utilizar o comando `- m` você pode inserir uma mensagem com a  descrição sobre o que foi feito ali. Assim essa modificação não fica perdida e todo mundo sabe do que se trata. Cada commit deve ser completo, porém mínimo. Algumas dicas úteis quando se trata das práticas de commits são: cada commit deve conter apenas mudanças de um problema específico, tornando-o mais fácil de entendimento, o commit deve resolver o problema ou buscar alterações específicas válidas, o commit deve descrever não tudo o que mudou, mas focar nas razões para as alterações feita e deve ser conciso.
[^4]: O comando `git push` empurra as suas modificações para o servidor, incluindo-as no histórico do projeto. Quando os outros integrantes da equipe fizerem um git pull, essas modificações serão baixadas e incluídas no repositório local da pessoa. Ao digitarmos o comando `git push -u origin master` estamos dizendo que estamos enviando os commits do branch atual para o branch master do repositório origin e queremos lembrar desses parâmetros para as próximas utilizações (através do comando `- u`). Assim, não precisaremos digitar estes parâmetros novamente na sessão e poderemos utilizar somente o comando `git push`.