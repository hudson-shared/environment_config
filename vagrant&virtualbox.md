### **VAGRANT E VIRTUALBOX**

### **VAGRANT**

Permite a criação rápida de ambientes virtuais. Existem dois atores que executam papéis distintos e muito importantes no Vagrant: 

* **provider** é responsável por criar uma instância de um ambiente, geralmente uma máquina virtual, com um sistema operacional básico instalado;
* **provisioner** é responsável por configurar o ambiente de maneira automatizada. 

Desta forma, usar o Vagrant nos permite ter um ambiente de desenvolvimento totalmente reproduzível e portátil

#### **NOSSO PROVIDER E PROVISIONER**

Há diversas opções de **provider** (VirtualBox, VmWare, AWS, Digital Ocean) e **provisioner** (Puppet, Chef, Ansible, Shell). Escolhi o uso do [VirtualBox](https://www.virtualbox.org/) como **provider** e o [Ansible](https://www.ansible.com/) como **provisioner**.

O VirtualBox é um virtualizador completo de uso geral para hardware x86. Voltado para utilização em servidores, desktops e dispositivos embarcados, ele é uma solução de virtualização com qualidade profissional, além de ser um software de código aberto. O VirtualBox roda no Windows, no Mac OS X, no Linux e no Solaris. Já o Ansible é é uma ferramenta Open Source em Python para automatizar ações em máquinas, seja local, máquinas virtuais, servidores próprios ou nuvens (Google, Azure, AWS, Open Stack, ...)

#### **INSTALAÇÃO**

Para instalar o Vagrant, baixe o pacote ou o instalador apropriado a partir da página de [download](https://www.vagrantup.com/downloads.html) e faça a instalação usando os procedimentos padrões do sistema operacional. 

Lembrando que, para criar suas máquinas vituais o Vagrant precisa do Virtual Box. Para instalar basta [baixar](https://www.virtualbox.org/wiki/Downloads) e seguir as instruções de download.

Para que tudo funcione, temos a necessidade de criar um arquivo chamado `Vagrantfile` que contem os detalhes para inicialização do ambiente e um `playbook` que contém tarefas para o provisionamento do ambiente. Estes arquivos estão no repositório de códigos de cada projeto de pesquisa. 

#### **COMANDOS BÁSICOS:**

* *vagrant up*: Cria e inicia a instancia após o comando vagrant init

* *vagrant suspend*: Comando para desligar a VM e salvar o estado em que ela estiver, quando inicializar a VM novamente, irá retornar ao mesmo estado

* *vagrant halt*: desliga a instancia ativa, finalizando todos os processos.

* *vagrant destroy*: Comando para parar a máquina em funcionamento e destruir todos os recursos que foram criados durante o processo de criação da máquina. Depois de executar este comando, o computador deve ficar em um estado limpo, como se você nunca tivesse criado a VM;

* *vagrant ssh*: Acessa a instancia ativa via ssh. 

* *vagrant status*: Informa o status atual da instancia

* *vagrant box list*: Lista suas box para utilização

* *vagrant box remove name*: Remove um box da lista de box

#### **CONFIGURAÇÃO**

Existem alguns níveis de configuração para o Vagrant. Abaixo, listamos cada um desses níveis:

* **config.vm**

As configurações dentro do `config.vm` modificam toda a parte de alocação de hardware e virtualização da VM.

* `config.vm.box`: Onde é configurado o box. Box é uma imagem que o Vagrant utiliza para clonar e criar a Virtual Machine, ao invés de termos que baixar manualmente. Há inúmeras distribuições de sistemas para usar como box, disponíveis no [link](https://atlas.hashicorp.com/boxes/search)
* `config.vm.network`: define configuração de rede para acessar o aplicativo pela virtual machine;
* `config.vm.provision`: define a configuração para o provisionamento que será feito;
* `config.vm.provider`: define configuração para o provider que será utilizado, default é o Virtual Box;
* `config.vm.synced_folder`: define quais pastas são sincronizadas com a VM, por default, a pasta que contém o  Vagrantfile já é sincronizada automaticamente sobre a pasta /vagrant/ na VM;
* `config.vm.communicator`: onde é configurado como será feita a comunicação entre a VM e a maquina local, por default, é utilizado ssh;
* `config.vm.post_up_message`: onde é configurado qual mensagem será apresentada após o comando vagrant up, pode ser muito útil para apresentar informações sobre como acessar todos os componentes do sistema;
 
    * **config.ssh**

As configurações dentro do config.ssh servem para configurar como o Vagrant irá acessar a VM via ssh.

* `config.ssh.username`: define qual usuário será configurado pra acessar a VM via ssh, por default, o usuário é o ‘vagrant’;
* `config.ssh.password`: define a senha para o usuário que for usado para conectar via ssh. É recomendável utilizar `private_key_path` para a configuração de senha;
* `config.ssh.sudo_command`: define que será por padrão a utilização do sudo nos comandos de shell;
 
    * **config.winrm**

As configurações dentro do config.winrm servem para configurar como o Vagrant irá acessar a VM com o Windows.

* `config.winrm.username`: define qual usuário será configurado pra acessar a VM via winrm, por default, o usuário é o ‘vagrant’;
* `config.winrm.password`: define a senha para o usuário que for usado para conectar via winrm, por default, a senha é ‘vagrant’;

#### **EXEMPLO:**

Você pode acessar um exemplo de Vagrantfile com algumas das opções acima neste [link](https://gitlab.com/hudson-papers/dsge-var/blob/master/ambiente/Vagrantfile). Lá você perceberá que há configurações específicas para o Ansible que é o provisioner.