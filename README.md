### **CONFIGURAÇÃO DO AMBIENTE**

Neste repositório você encontrará códigos que instalam requisitos necessários para qualquer um dos meus projetos. Seguindo os passos abaixo, você estará apto para replicar a pesquisa em seu computador ou servidor. 

Em linhas gerais, você precisará de três tecnologias instaladas em seu computador/notebook para que a configuração do seu ambiente ocorra naturalmente. Estou falando do **Git, VirtualBox e Vagrant**. O [Git](https://git-scm.com/) será nosso versionador de códigos enquanto o [VirtualBox](https://www.virtualbox.org/) será o virtualizador do ambiente. Já o [Vagrant](https://www.vagrantup.com/) é o responsável por executar as tarefas de virtualização (usando o VirtualBox) tornando o ambiente portátil. 

Nos links abaixo, apresento brevemente como instalar em seu computador/notebook cada uma destas tecnologias seja no Windows ou Linux (aqui, Ubuntu 16-04) e alguns comandos importantes. 

 * [Git](https://gitlab.com/hudson-shared/environment_config/blob/master/git.md#markdown-header-usando-o-git)
 * [Vagrant e VirtualBox](https://gitlab.com/hudson-shared/environment_config/blob/master/vagrant&virtualbox.md)


